module codeberg.org/gruf/go-cache/v3

go 1.20

require (
	codeberg.org/gruf/go-maps v1.0.4
	codeberg.org/gruf/go-sched v1.2.4
	github.com/google/go-cmp v0.6.0
)

require (
	codeberg.org/gruf/go-byteutil v1.2.0 // indirect
	codeberg.org/gruf/go-errors/v2 v2.3.2 // indirect
	codeberg.org/gruf/go-kv v1.6.5 // indirect
	codeberg.org/gruf/go-runners v1.6.3 // indirect
)
